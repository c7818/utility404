#include "string_format.hpp"

int main() {

	std::string s = "a";
	const std::string cs = "b";
	const char* cc = "c";

	int i = 1;
	const int i2 = 2;

	using namespace std::literals;
	std::cout << string_format("%s %s %s %s %s %d %d %d \n", s, cs, cc, "d", "e"s, i, i2, 3);

	return 0;
}