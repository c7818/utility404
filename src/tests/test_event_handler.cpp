#include <functional>
#include <iostream>
#include <memory>

#include "EventHandler.hpp"

using namespace utility404;

struct event {
private:
	virtual void dummy(){}
};

struct eventA : public event {};
struct eventB : public event {};

int main(int argc, char const *argv[])
{
	EventHandler<event> eh;

	std::function<void(std::shared_ptr<eventA> e)> fa = [](std::shared_ptr<eventA> e){std::cout << "EventA observer" << std::endl;};
	eh.registerObserver(fa);
	std::function<void(std::shared_ptr<eventA> e)> fa2 = [](std::shared_ptr<eventA> e){std::cout << "EventA observer2" << std::endl;};
	eh.registerObserver(fa2, 12);
	std::function<void(std::shared_ptr<eventB> e)> fb = [](std::shared_ptr<eventB> e){std::cout << "EventB observer" << std::endl;};
	eh.registerObserver(fb);
	std::function<void(std::shared_ptr<event> e)> f = [](std::shared_ptr<event> e){std::cout << "Event observer" << std::endl;};
	eh.registerObserver(f, 11);

	std::cout << "posting event" << std::endl;
	eh.postEvent(std::make_shared<event>());
	std::cout << "posting eventA" << std::endl;
	eh.postEvent(std::make_shared<eventA>());
	std::cout << "posting eventB" << std::endl;
	eh.postEvent(std::make_shared<eventB>());

	return 0;
}