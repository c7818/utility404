#include "DispatchQueue.hpp"

namespace utility404 {

	DispatchQueue::DispatchQueue() {
		m_Thread = std::thread(&DispatchQueue::handler, this);
	}

	DispatchQueue::~DispatchQueue() {
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Quit = true;
		lock.unlock();
		m_Cond.notify_one();

		if(m_Thread.joinable())
			m_Thread.join();
	}

	void DispatchQueue::dispatch(const task& op)
	{
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Queue.push(op);
		lock.unlock();
		m_Cond.notify_one();
	}

	void DispatchQueue::dispatch(task&& op)
	{
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Queue.push(std::move(op));
		lock.unlock();
		m_Cond.notify_one();
	}

	void DispatchQueue::addTimer(const Timer::TimerInfo &info){
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Timers.push(info);
		m_Queue.push([](){});
		lock.unlock();
		m_Cond.notify_one();
	}

	void DispatchQueue::removeTimer(const Timer::TimerInfo &info){
		std::unique_lock<std::mutex> lock(m_Lock);
		
		std::priority_queue<Timer::TimerInfo> tmp_timers;
		while(!m_Timers.empty()){
			if (m_Timers.top().id != info.id)
				tmp_timers.push(m_Timers.top());
			m_Timers.pop();
		}
		while(!tmp_timers.empty()){
			m_Timers.push(tmp_timers.top());
			tmp_timers.pop();
		}

		lock.unlock();
	}

	void DispatchQueue::handler(void)
	{
		std::unique_lock<std::mutex> lock(m_Lock);

		do {
			if (m_Timers.empty())
				m_Cond.wait(lock, [this]{return (m_Queue.size() != 0 || m_Quit);});
			else
				m_Cond.wait_until(lock, m_Timers.top().timeout, [this]{return (m_Queue.size() != 0 || m_Quit);});

			if(!m_Quit)
			{
				while (m_Queue.size()) {
					auto op = std::move(m_Queue.front());
					m_Queue.pop();

					lock.unlock();

					op();

					lock.lock();
				}
				while (!m_Timers.empty() && m_Timers.top().timeout <= Timer::ClockType::now()) {
					Timer::TimerInfo info = m_Timers.top();
					m_Timers.pop();

					lock.unlock();

					info.id->process();

					lock.lock();
				}
			}
		} while (!m_Quit);
	}

}