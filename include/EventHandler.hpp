#pragma once

#include <functional>
#include <list>
#include <memory>
#include <queue>
#include <typeindex>
#include <typeinfo>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>


namespace utility404 {

	/**
	 * @brief      This class describes an event handler.
	 *
	 * @details    Observer can be added to react to specific event.
	 */
	template <class event,
	typename std::enable_if<std::is_polymorphic<event>::value, int>::type = 0>
	class EventHandler
	{
	public:
		using task = std::function<void(std::shared_ptr<event>)>;

	public:
		/**
		 * @brief      Constructs a new instance.
		 */
		EventHandler() {}
		/**
		 * @brief      Destroys the object.
		 */
		~EventHandler() {}


		/**
		 * @brief      Register a new observer to react to a specific event.
		 *
		 * @param[in]  f          The task to execute when a corresponding event is
		 *                        posted. This function takes as parameter a smart
		 *                        pointer to the event to look for.
		 * @param[in]  prio       The priority of the observer. A highier priority
		 *                        means that the observer will be called first.
		 *
		 * @tparam     EventType  The type of event to watch for.
		 */
		template <class EventType,
		typename std::enable_if<std::is_base_of<event, EventType>::value, int>::type = 0>
		void registerObserver(std::function<void(std::shared_ptr<EventType>)> f, int prio = 10)
		{
			auto it = m_map.find(std::type_index(typeid(EventType)));
			if (it != m_map.end())
			{
				item i{prio, [=](std::shared_ptr<event> e){
					f(std::static_pointer_cast<EventType>(e));
				}};
				it->second.second.push_back(i);
				return;
			}

			checker c = [](std::shared_ptr<event> e)->bool {
				return (bool)std::dynamic_pointer_cast<EventType>(e);
			};

			m_map.emplace(std::piecewise_construct,
				std::forward_as_tuple(std::type_index(typeid(EventType))),
				std::forward_as_tuple(c, std::list<item>()));

			item i{prio, [=](std::shared_ptr<event> e){
				f(std::static_pointer_cast<EventType>(e));
			}};
			m_map[std::type_index(typeid(EventType))].second.push_back(i);
		}

		/**
		 * @brief      Posts an event.
		 *
		 * @param[in]  e     A smart pointer to the event to post.
		 */
		void postEvent(std::shared_ptr<event> ev)
		{
			task_priority_queue q;

			for (auto [e, p] : m_map)
			{
				if(p.first(ev))
				{
					for (auto i : p.second)
					{
						q.push(i);
					}
				}
			}

			while (!q.empty()){
				q.top()._task(ev);
				q.pop();
			}
		}

	private:
		EventHandler(const EventHandler&) = delete;
		EventHandler(EventHandler&&) = delete;
		EventHandler& operator=(const EventHandler&) = delete;
		EventHandler& operator=(EventHandler &&) = delete;

	private:
		using checker = std::function<bool(std::shared_ptr<event>)>;

		struct item {
			int _priority;
			task _task;
		};

		struct cmp_item { 
			bool operator()(item const& left, item const& right) 
			{
				return (left._priority) < (right._priority); 
			} 
		};

		using task_priority_queue = std::priority_queue<item, std::vector<item>, cmp_item>;

	private:
		std::unordered_map< std::type_index, std::pair<checker, std::list<item>> > m_map;

	};

} // utility404