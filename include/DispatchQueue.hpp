#pragma once

#include "Timer.hpp"

#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>

namespace utility404 {

	/**
	 * @brief      Class defining a dispatch queue
	 *
	 *             A dispatch queue is queue of tasks to do. Tasks can be posted
	 *             here for the internal thread to execute them.
	 */
	class DispatchQueue {
		using task = std::function<void(void)>; ///< Type of the task that can be executed

		friend class Timer;

	public:
		/**
		 * @brief      Constructs the object.
		 */
		DispatchQueue();
		/**
		 * @brief      Destroys the object.
		 */
		~DispatchQueue();

		/**
		 * @brief      Dispatch a task to the internal thread.
		 *
		 * @param[in]  op    The task to do
		 */
		void dispatch(const task& op);
		/**
		 * @brief      Dispatch a task to the internal thread.
		 *
		 * @param[in]  op    The task to do
		 */
		void dispatch(task&& op);

	private:
		/**
		 * @brief      Adds a timer to the dispatch queue.
		 *
		 * @param[in]  info  The TimerInfo object associated to a timer.
		 */
		void addTimer(const Timer::TimerInfo &info);
		/**
		 * @brief      Removes a timer from the dispatch queue.
		 *
		 * @param[in]  info  The TimerInfo object associated to a timer.
		 */
		void removeTimer(const Timer::TimerInfo &info);

		DispatchQueue(const DispatchQueue& rhs) = delete;
		DispatchQueue& operator=(const DispatchQueue& rhs) = delete;
		DispatchQueue(DispatchQueue&& rhs) = delete;
		DispatchQueue& operator=(DispatchQueue&& rhs) = delete;

	private:
		std::mutex								m_Lock; ///< Mutex for thread safety.
		std::thread								m_Thread; ///< Internal thread.
		std::queue<task>						m_Queue; ///< The queue of tasks.
		std::condition_variable					m_Cond; ///< Condition to wakeup the thread.
		std::priority_queue<Timer::TimerInfo> 	m_Timers; ///< Priority queue containing the timers ordered.
		bool									m_Quit = false; ///< Should the thread stops.

		/**
		 * @brief      Main function of the thread.
		 */
		void handler(void);
	};

}