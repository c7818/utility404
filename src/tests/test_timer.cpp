#include <Timer.hpp>
#include <DispatchQueue.hpp>
#include <iostream>

int main(int argc, char const *argv[])
{
	utility404::DispatchQueue dq;

	utility404::Timer t1([](){std::cout << "t1" << std::endl;}, dq);
	utility404::Timer t2([](){std::cout << "t2" << std::endl;}, dq);

	t1.oneshot(std::chrono::milliseconds(2000));
	t2.start(std::chrono::milliseconds(500));

	std::this_thread::sleep_for (std::chrono::seconds(3));

	t2.stop();

	std::this_thread::sleep_for (std::chrono::seconds(1));

	return 0;
}