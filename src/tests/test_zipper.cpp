#include<iostream>
#include<vector>
#include<list>
#include<algorithm>
#include<array>
#include"Zipper.hpp"

struct A {
    A() {std::cout << "Default" << std::endl;}
    A(const A&) {std::cout << "Copy" << std::endl;}
    A(A&&) {std::cout << "Move" << std::endl;}
    A& operator=(const A&) {std::cout << "=Copy" << std::endl; return *this;}
    A& operator=(A&&) {std::cout << "=Move" << std::endl; return *this;}

    int a = 0;
};

std::vector<A> foo() {return std::vector<A>(2);}

int main( )
{
    std::vector<double> a{1.0, 2.0, 3.0, 4.0};
    std::list<char> b;
    b.push_back('a');
    b.push_back('b');
    b.push_back('c');
    b.push_back('d');
    const std::array<int,5> c{5,4,3,2,1};

    auto d = zip(a, b, c);

    for (auto i : zip(a, b, c) )
    {
        std::cout << std::get<0>(i) << ", " << std::get<1>(i) << ", " <<
        std::get<2>(i) << std::endl;
    }

    for (auto i : d)
    {
        std::cout << std::get<0>(i) << ", " << std::get<1>(i) << ", " <<
        std::get<2>(i) << std::endl;
        std::get<0>(i) = 5.0;
    }
    for (auto i : d)
    {
        std::cout << std::get<0>(i) << ", " << std::get<1>(i) << ", " <<
        std::get<2>(i) << std::endl;
    }

    std::vector<A> tt(2);
    for (auto [i, j] : zip(tt, foo()))
    {
        std::cout << i.a << " " << j.a << std::endl;
    }

    return 0;
}
