#pragma once

#include <chrono>
#include <exception>
#include <functional>
#include <mutex>

/**
 * @brief      Structure implementing an std::exception specific to timers
 */
struct timer_already_running : public std::exception {
	/**
	 * @brief      Return a string describing the error.
	 *
	 * @return     a string describing the error.
	 */
	const char *what() const throw () {
		return "Timer is already running";
	}
};

namespace utility404 {

	class DispatchQueue;

	/**
	 * @brief      Class for timer.
	 *
	 *             This class will use a dispatch to wait the needed time and
	 *             then launch the action associated to it.
	 */
	class Timer {
		friend class DispatchQueue;
	public:
		using ClockType = std::chrono::steady_clock; ///< type of clock used by the timer
		using TimeType = ClockType::time_point; ///< type of time point used by the timer
		using TimeoutFct = std::function<void(void)>; ///< type of function to execute when the timer end

		/**
		 * @brief      Constructs the object.
		 *
		 * @param[in]  task   The task to do when timeout
		 * @param      queue  The dispatch queue to use for the execution of the task
		 */
		Timer(const TimeoutFct &task, DispatchQueue &queue);
		/**
		 * @brief      Constructs the object (move constructor).
		 *
		 * @param      task   The task to do when timeout
		 * @param      queue  The dispatch queue to use for the execution of the task
		 */
		Timer(TimeoutFct &&task, DispatchQueue &queue);
		/**
		 * @brief      Destroys the object.
		 */
		~Timer();

		/**
		 * @brief      Launch the timer for a single execution
		 *
		 * @param[in]  interval  The interval of time to wait for
		 */
		void oneshot(const std::chrono::milliseconds &interval);
		/**
		 * @brief      Launch the timer for a single execution
		 *
		 * @param[in]  interval  The interval of time to wait for
		 */
		void oneshot(std::chrono::milliseconds &&interval);
		/**
		 * @brief      Launch the timer for a periodic execution
		 *
		 * @param[in]  interval  The interval of time to wait for
		 */
		void start(const std::chrono::milliseconds &interval);
		/**
		 * @brief      Launch the timer for a periodic execution
		 *
		 * @param[in]  interval  The interval of time to wait for
		 */
		void start(std::chrono::milliseconds &&interval);
		/**
		 * @brief      Stop the timer
		 */
		void stop();

		/**
		 * @brief      Structure holding the information about the timer
		 */
		struct TimerInfo {
			TimeType timeout; ///< Time point for the next execution
			Timer *id; ///< Pointer to the timer
			/**
			 * @brief      Compare to time info according to their timeout
			 *             timepoint
			 *
			 * @param[in]  l     The left TimerInfo
			 * @param[in]  r     The right TimerInfo
			 *
			 * @return     return true if the left TimerInfo will finish later.
			 */
			friend bool operator<(const TimerInfo& l, const TimerInfo& r)
			{
				return l.timeout > r.timeout;
			}
		};

	private:
		/**
		 * @brief      Execute the task
		 */
		void process();

		Timer() = delete;
		Timer(const Timer&) = delete;
		Timer(Timer&&) = delete;
		Timer &operator=(const Timer&) = delete;
		Timer &operator=(Timer&&) = delete;

	private:
		std::mutex			m_Lock; ///< Mutex used for thread safety
		const TimeoutFct	m_Task; ///< Task to execute on timeout
		DispatchQueue  	   &m_Queue; ///< Dispatch queue to use
		TimerInfo			m_Info; ///< The TimerInfo associated to the Timer
		bool				m_Running; ///< Is the timer running
		bool				m_Oneshot; ///< Is the timer a oneshot

		std::chrono::milliseconds	m_Timeout; ///< Time to wait before each execution
	};

}