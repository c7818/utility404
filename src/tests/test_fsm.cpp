#include <iostream>
#include <string>

#include <FiniteStateMachine.hpp>

struct event {
private:
	virtual void dummy(){}
};
struct eventFSM : public event {};
struct eventS12: public eventFSM {};
struct eventS21: public eventFSM {
	eventS21(bool b) : valid(b) {}
	bool valid;
};
struct eventS34: public eventFSM {};
struct eventS43: public eventFSM {};
struct eventS56: public eventFSM {};
struct eventS65: public eventFSM {};

class CoutState : public utility404::State<eventFSM>
{
public:
	CoutState(std::string name, bool mem = false) : m_name(name), utility404::State<eventFSM>(mem) {}
	virtual void onEntry(std::shared_ptr<eventFSM> e)
	{
		std::cout << "On entry: " << m_name << std::endl;
	}
	virtual void onExit(std::shared_ptr<eventFSM> e)
	{
		std::cout << "On exit: " << m_name << std::endl;
	}
private:
	std::string m_name;
};

int main(int argc, char const *argv[])
{
	utility404::StateMachine<eventFSM> sm;

	CoutState *s1 = new CoutState("s1", true), *s2 = new CoutState("s2"), *s3 = new CoutState("s3"), *s4 = new CoutState("s4"), *s5 = new CoutState("s5"), *s6 = new CoutState("s6");

	s1->addTransition<eventS12>(s2, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s1 to s2." << std::endl;});
	s2->addTransition<eventS21>(s1, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s2 to s1." << std::endl;},
		[](std::shared_ptr<eventFSM> e) -> bool {
			std::cout << "Transition from s2 to s1: " << (std::static_pointer_cast<eventS21>(e)->valid ? "OK" : "bad") << std::endl;
			return std::static_pointer_cast<eventS21>(e)->valid;});
	s3->addTransition<eventS34>(s4, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s3 to s4." << std::endl;});
	s4->addTransition<eventS43>(s3, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s4 to s3." << std::endl;});
	s5->addTransition<eventS56>(s6, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s5 to s6." << std::endl;});
	s6->addTransition<eventS65>(s5, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s6 to s5." << std::endl;});

	s1->addState(s5);
	s1->addState(s6);
	s1->setInitialState(s5);

	s2->addState(s3);
	s2->addState(s4);
	s2->setInitialState(s3);

	sm.addState(s1);
	sm.addState(s2);

	sm.setInitialState(s1);

	sm.setDefaultAction([](std::shared_ptr<event> e){std::cout << "Ignoring event." << std::endl;});

	sm.processEvent(std::make_shared<eventS56>());
	sm.processEvent(std::make_shared<eventS65>());
	sm.processEvent(std::make_shared<eventS56>());
	sm.processEvent(std::make_shared<eventS12>());
	sm.processEvent(std::make_shared<eventS12>());
	sm.processEvent(std::make_shared<eventS34>());
	sm.processEvent(std::make_shared<eventS43>());
	sm.processEvent(std::make_shared<eventS34>());
	sm.processEvent(std::make_shared<eventS21>(false));
	sm.processEvent(std::make_shared<eventS21>(true));
	sm.processEvent(std::make_shared<eventS12>());

	return 0;
}