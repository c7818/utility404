#pragma once

#include <list>
#include <map>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <typeindex>
#include <utility>


namespace utility404 {

	template <typename Key, typename... Ts>
	class Factory {
	public:
		~Factory() {}

		static Factory &GetInstance() noexcept {
			return m_Singleton;
		}

		template<typename T>
		T Get(Key name) {
			return get<T>(name, std::index_sequence_for<Ts...>{});
		}

		void Register(Key name, Ts... datas) {
			m_Maps[name] = std::make_tuple(datas...);
		}

		std::list<Key> GetListType( void ) {
			std::list<Key> ret;
			for (auto s : m_Maps)
				ret.push_back(s.first);
			return ret;
		}

	private:
		Factory() {}
		static Factory m_Singleton;

		template<typename T, std::size_t... Is>
		T get(Key name, std::index_sequence<Is...>) {
			size_t i = ((std::is_same<T, typename std::tuple_element<Is, std::tuple<Ts...>>::type>::value ? Is + 1 : 0)+ ...) - 1;
			void *add[] = {&std::get<Is>(m_Maps[name]) ...};
			return *static_cast<T*>(add[i]);
		}

		std::map<Key, std::tuple<Ts...>> m_Maps;

	private:
		Factory(const Factory&) = delete;
		Factory& operator=(const Factory&) = delete;

	};

	template <typename Key, typename... Ts>
	Factory<Key, Ts...> Factory<Key, Ts...>::m_Singleton;

} // utility404