#include "Factory.hpp"

#include <iostream>
#include <string>


using type1 = std::string;
using type2 = int;
using type3 = unsigned;

using MyFactory = utility404::Factory<
	unsigned char,
	type1,
	type2,
	type3
>;

int main(int argc, char const *argv[])
{
	MyFactory::GetInstance().Register(1, "type1", -1, 1);
	MyFactory::GetInstance().Register(2, "type2", -2, 2);
	std::cout << "Key = 1: " << MyFactory::GetInstance().Get<type1>(1) << ", " << MyFactory::GetInstance().Get<type2>(1) << ", " << MyFactory::GetInstance().Get<type3>(1) << std::endl;
	std::cout << "Key = 2: " << MyFactory::GetInstance().Get<type1>(2) << ", " << MyFactory::GetInstance().Get<type2>(2) << ", " << MyFactory::GetInstance().Get<type3>(2) << std::endl;
	return 0;
}