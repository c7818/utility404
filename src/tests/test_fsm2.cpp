#include <iostream>
#include <string>

#include <FiniteStateMachine.hpp>

struct event {
private:
	virtual void dummy(){}
};
struct eventFSM : public event {};
struct eventS12: public eventFSM {
	eventS12(bool b) : valid(b) {}
	bool valid;
};
struct eventS21: public eventFSM {};

class CoutState : public utility404::State<eventFSM>
{
public:
	CoutState(std::string name, bool mem = false) : m_name(name), utility404::State<eventFSM>(mem) {}
	virtual void onEntry(std::shared_ptr<eventFSM> e)
	{
		std::cout << "On entry: " << m_name << std::endl;
	}
	virtual void onExit(std::shared_ptr<eventFSM> e)
	{
		std::cout << "On exit: " << m_name << std::endl;
	}
private:
	std::string m_name;
};

int main(int argc, char const *argv[])
{
	utility404::StateMachine<eventFSM> sm;

	CoutState *s1 = new CoutState("s1", true), *s2b = new CoutState("s2bad"), *s2g = new CoutState("s2good");

	s1->addTransition<eventS12>(s2b, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s1 to s2bad." << std::endl;},
		[](std::shared_ptr<eventFSM> e) -> bool {
			return !std::static_pointer_cast<eventS12>(e)->valid;});
	s1->addTransition<eventS12>(s2g, [](std::shared_ptr<event> e){std::cout << "Transition from s1 to s2good." << std::endl;},
		[](std::shared_ptr<eventFSM> e) -> bool {
			return std::static_pointer_cast<eventS12>(e)->valid;});
	s2b->addTransition<eventS21>(s1, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s2bad to s1." << std::endl;});
	s2g->addTransition<eventS21>(s1, [](std::shared_ptr<eventFSM> e){std::cout << "Transition from s2good to s1." << std::endl;});

	sm.addState(s1);
	sm.addState(s2b);
	sm.addState(s2g);

	sm.setInitialState(s1);

	sm.setDefaultAction([](std::shared_ptr<eventFSM> e){std::cout << "Ignoring event." << std::endl;});

	sm.processEvent(std::make_shared<eventS12>(false));
	sm.processEvent(std::make_shared<eventS21>());
	sm.processEvent(std::make_shared<eventS12>(true));
	sm.processEvent(std::make_shared<eventS21>());

	return 0;
}